# Envoy Challenge
Taking a gulp of bourbon, neat.

## Installation
1. `npm install`
2. `bower install`
3. `gulp build` to build
4. `gulp serve` for dev

## Requirements
1. Node
2. Bower
3. Gulp
