$(function(){

  // Twitter Button
  $('.btn-twitter').click(function(e){
    e.preventDefault();
    window.open($(this).attr('href'), '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
    return false;
  });

  // Mobile Menu
  $('.btn-mobile-menu').click(function(e){
    e.preventDefault();
    $(this).toggleClass('is-active');
    $('.menu-main, #canvas').toggleClass('toggled');
  });

  // Lazy Load Images
  $('.lazy').lazyload({
    threshold : 200
  });

});
