$(function(){

  /* Settings
  ---------------------------------------*/
  let slideShow = $('.wrap-slideshow');
  let slideAnimPrefix = 'slide-'; // the prefix of the slide to animate
  let slideAnimElem = '.slide-reveal'; // the class of the slide elements to animate

  /* Methods
  ---------------------------------------*/
  let evSlider = {
    findTarget: target => {
      let result = $('[data-slick-index=\''+target+'\']').attr('class').split(' ');
      for (var i = 0; i < result.length; i++){
        if(result[i].indexOf(slideAnimPrefix) !== -1){
          return result[i];
        }
      }
    },
    showSlide: currTarget => {
      TweenMax.set($(slideAnimElem), {
        opacity:0
      });
      TweenMax.staggerTo($('.' + currTarget + ' ' + slideAnimElem), 1, {
        opacity:1
      }, .4);
    }
  }

  /* Event Listeners
  ---------------------------------------*/
  // Show the Initial Slide
  slideShow.on('init', function(event, slick){
    let currTarget = evSlider.findTarget(0);
    evSlider.showSlide(currTarget);
  });

  // Build Slideshow
  slideShow.slick({
    dots: true,
    prevArrow:'<div class=\'slick-nav nav-prev\'></div>',
    nextArrow:'<div class=\'slick-nav nav-next\'></div>',
    appendArrows:$('.slick-nav-cont'),
    autoplay: true,
    autoplaySpeed: 7000,
    fade: true,
    cssEase: 'linear'
  });

  // Slides Transitions
  slideShow.on('afterChange', function(event, slick, currentSlide){
    let currTarget = evSlider.findTarget(currentSlide);
    evSlider.showSlide(currTarget);
  });

});
